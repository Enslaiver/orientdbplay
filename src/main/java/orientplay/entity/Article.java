package orientplay.entity;

import com.orientechnologies.orient.core.annotation.OVersion;

import java.util.List;

/**
 * extends OrientVertex for extending from V
 * <p/>
 * User: akataev
 * Date: 30.11.14
 * Time: 21:52
 */
public class Article {

    @OVersion
    private Object version;

    private String title;

    private String content;

    private List<Member> authors;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Member> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Member> authors) {
        this.authors = authors;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + getTitle() + '\'' +
                ", content='" + content + '\'' +
                ", authors=" + getAuthors() +
                '}';
    }
}
