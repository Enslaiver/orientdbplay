package orientplay.entity;

import java.util.List;

/**
 * User: akataev
 * Date: 30.11.14
 * Time: 22:26
 */
public class Member {
    private String name;

    private List<Article> articles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Member{" +
                "name='" + getName() + '\'' +
                '}';
    }
}
